import { getRouteGenerator, passwordValidator } from 'controller/helper'
import httpStatusCodes from 'http-status-codes'
import { allowResetPassword } from 'middleware'
import { RouterFactory, ServicesRoute } from 'types'

/* code snippet shows just a single endpoint */
const resetPassword: ServicesRoute<{ password: string }, void, {}> = services => async (
  request,
  response,
) => {
  if (!passwordValidator(request.body.password)) {
    response.status(httpStatusCodes.BAD_REQUEST).send({
      error: passwordValidator.errors.map(error => `password - ${error.message}`).join(' / '),
    })
    return
  }

  const result = await services.auth.resetPassword({
    userId: request.user.id,
    password: request.body.password,
  })

  if (!result) {
    response.status(httpStatusCodes.NOT_FOUND).send('User not found')
    return
  }

  response.clearCookie('token')
  response.sendStatus(httpStatusCodes.OK)
}

const initOnboardRoutes: RouterFactory = (router, services) => {
  const route = getRouteGenerator(router)

  route('patch', '/reset-password', allowResetPassword(services), resetPassword(services))
}

export default initOnboardRoutes
