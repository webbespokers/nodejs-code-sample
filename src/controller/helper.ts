import { Router } from 'express'
import { HttpMethod, Route } from 'types'

/* code snippet shows just a single helper function */
export const getRouteGenerator = (router: Router) => (
  method: HttpMethod,
  path: string,
  ...middleware: Array<Route | Route[]>
) => {
  const routerMethod = router[method]
  const handledMiddleware = middleware.flat().map(handleRouteError)
  return routerMethod.call(router, path, handledMiddleware) as ReturnType<typeof routerMethod>
}
