import bcrypt from 'bcrypt'
import Database from 'database'
import { OAuth2Client } from 'google-auth-library'
import { AccessTokenType } from 'model/AccessToken'
import { IAuthConfig } from 'types'

/* code snippet shows a few methods only */
class Auth {
  public static hashPassword(password: string) {
    return bcrypt.hash(password, 10)
  }
  private config: IAuthConfig
  private database: Database

  constructor({ config, database }: { config: IAuthConfig; database: Database }) {
    this.config = config
    this.database = database
  }

  public async resetPassword({ userId, password }: { userId: number; password: string }) {
    const user = await this.database.collections.user.findOne({
      where: { id: userId, isActive: true },
    })

    if (!user) {
      return false
    }

    const passwordHash = await Auth.hashPassword(password)
    await user.update({ password: passwordHash })
    await this.destroyUserAccessTokens(userId, 'RESET_PASS')
    return true
  }

  public async googleAuth(token: string) {
    const googleAuthClient = new OAuth2Client(
      this.config.googleAuthConfig.clientId,
      this.config.googleAuthConfig.clientSecret,
    )

    const ticket = await googleAuthClient.verifyIdToken({
      idToken: token,
      audience: this.config.googleAuthConfig.clientId,
    })

    return ticket.getPayload()
  }

  private async destroyUserAccessTokens(userId: number, type?: AccessTokenType) {
    const where = type ? { userId, type } : { userId }
    const userAccessTokens = await this.database.collections.accessToken.findAll({
      where,
    })
    const pendingDeletions = userAccessTokens.map(accessToken => accessToken.destroy())
    await Promise.all(pendingDeletions)
  }
}

export default Auth
