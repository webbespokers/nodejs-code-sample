import { initRoutes } from 'controller'
import { runPopulateHelper } from 'database/faker'
import { initServices } from 'services'

const initApp = async () => {
  const services = await initServices()
  // used only in dev mode
  await runPopulateHelper(services)
  initRoutes(services)
  services.app.setUnhandledRoutes()

  await services.app.runServer()
  console.log(`[Server] listening on port ${services.app.port} in ${services.app.env} mode`)
}

initApp()
